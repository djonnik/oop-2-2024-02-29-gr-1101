def my_counter(string: str) -> int:
    return 0


if __name__ == "__main__":
    cases = [
        ("", 0),
        ("abbbccdf", 3),
        ("12bvccdd66p", 5),
        ("/sqq  ety667b/\\", 7),
    ]
    for arg, result in cases:
        assert my_counter(arg) == result, f"expected: my_counter({arg}) == {result}, got: my_counter({arg}) == {my_counter(arg)}"
